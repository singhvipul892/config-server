FROM openjdk:8-jdk-alpine
MAINTAINER vipcoder
WORKDIR /home/appuser
COPY target/config-server-0.0.1-SNAPSHOT.jar  config-server-0.0.1-SNAPSHOT.jar
COPY UnlimitedJCEPolicyJDK8/*  /usr/lib/jvm/java-1.8-openjdk/jre/lib/security/
ENTRYPOINT ["/usr/bin/java"]
CMD ["-jar", "config-server-0.0.1-SNAPSHOT.jar"]
VOLUME /var/lib/config-repo
EXPOSE 9090
